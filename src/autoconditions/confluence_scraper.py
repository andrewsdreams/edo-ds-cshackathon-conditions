import requests

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

class Scrape:
    def __init__(self, credentials):

        self.image_dir = "data/images/"

        options = webdriver.ChromeOptions()
        options.add_argument('headless')

        self.driver = webdriver.Chrome(chrome_options=options)
        self.driver.get("https://jira.odigeo.com/wiki/display/CS/COVID-19")

        self.session = None

        if len(self.driver.find_elements_by_id("captcha-container")) != 0:
            raise CaptchaError("Perform a manual login to get rid of captchas")
        else:
            uname = self.driver.find_element_by_id("os_username")
            uname.send_keys(credentials["username"])
            pword = self.driver.find_element_by_id("os_password")
            pword.send_keys(credentials["password"])
            pword.send_keys(Keys.RETURN)

    def getAirlinePages(self):
        text = self.driver.find_element(By.XPATH, "//u[text()='Click below to check the policy of each airline:']")
        nav = text.find_element(By.XPATH, "./..").find_element(By.XPATH, "following-sibling::*").find_element(By.XPATH, "following-sibling::*")
        i = 0
        airlines = {}
        for e in nav.find_elements_by_tag_name("a"):
            i +=1
            if i <= 2:
                continue
            url = e.get_attribute("href")
            name = ""
            siblings = 1
            top = e.find_element(By.XPATH, "./..")
            while top.tag_name != "td":
                top = top.find_element(By.XPATH, "./..")
            children = top.find_elements(By.XPATH, ".//*")
            for c in children:
                if name != "":
                    break
                while "<" in c.get_attribute("innerHTML"):
                    c = c.find_element(By.XPATH, ".//*")
                name = c.get_attribute("innerHTML")
            airlines[name] = url
        return airlines

    def getConditions(self, carrier, url, credentials):
        self.driver.get(url)
        images = self.driver.find_elements_by_class_name("confluence-embedded-image")
        if len(images) > 1:
            info = images[1]
            imgsrc = info.get_attribute("src")
            if self.session is None:
                self.session = requests.Session()
                for c in self.driver.get_cookies():
                    self.session.cookies.set(c['name'], c['value'])

            result = self.session.get(imgsrc, stream=True)
            if result.status_code == 200:
                with open(self.image_dir + carrier + ".png", 'wb') as f:
                    for chunk in result:
                        f.write(chunk)
                return self.image_dir + carrier + ".png"
            else:
                return None
        else:
            return None

class CaptchaError(Exception):
    pass