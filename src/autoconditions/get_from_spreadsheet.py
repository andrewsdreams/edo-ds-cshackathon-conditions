import openpyxl
import datetime
import logging
import numbers
import dateparser

import pandas as pd

from dateparser.search import search_dates
from datetime import datetime, date
from collections import OrderedDict
from itertools import islice
from django.core.validators import URLValidator, ValidationError


class UnknownColorError(Exception):
    pass


def get_xls():
    wb = openpyxl.load_workbook('data/policies.xlsx')
    ignore = ['TO REVIEW', 'airlines', 'do not touch', 'Instructions', 'Form responses 1', 'airlines', 'TEMPLATE', "IATA's & EMD", 'QR---', 'EK++', 'CI (old)', 'matrix']
    obsolete_colors = ['FFD9D9D9', 'FFCCCCCC', 'FFB7B7B7', 'FF999999', 'FF666666', 'FFFFFFFF']
    emd_colors = ['FF45818E']

    approved_colors = ['FF3D85C6', 'FFB6D7A8', 'FFD9EAD3', 'FF9900FF', '00000000', 'FF93C47D', 'FF6FA8DC', 'FF3C78D8', 'FF6D9EEB']

    airline_infos = {}
    airline_links = {}
    logging.info("Starting processing")

    for carrier in wb.sheetnames:
        if carrier in ignore:
            logging.info("Ignoring tab: {}...".format(carrier))
            continue
        else:
            logging.info("Processing tab: {}...".format(carrier))
            tables = []
            page = wb[carrier]

            links = []
            for line in page.iter_rows():
                remains = [cell.value for cell in line]
                offset = 0

                #get the tables of interest
                while 'LAST UPDATE' in remains:
                    i = remains.index('LAST UPDATE')
                    color = line[i + offset].fill.start_color.rgb

                    if color in obsolete_colors or color in emd_colors:
                        offset += i+1
                        remains = remains[i+1:]
                        continue
                    elif color not in approved_colors:
                        print(carrier, color)
                        raise UnknownColorError

                    for j, cell in enumerate(remains[i+1:]):
                        if cell == "LAST UPDATE":
                            break

                        if isinstance(cell, date) or (isinstance(cell, str) and dateparser.parse(cell) is not None):
                            wrapper = line[i + j + offset + 1]
                            value_color = wrapper.fill.start_color.rgb
                            if value_color not in obsolete_colors and value_color not in emd_colors:
                                tables.append((wrapper.row, i + offset, i + j + offset+1))
                                break

                    offset += i+1
                    remains = remains[i+1:]

                remains = [cell.value for cell in line]
                remains_enum = enumerate(remains)
                val = URLValidator()

                #separately get the link to the airline info. This should be unique per page
                for i, cell in remains_enum:
                    if isinstance(cell, str) and cell in ['LINK', 'LINK :', 'Link', 'Link :', 'Link:']:
                        for j, cell in enumerate(remains[i+1:]):
                            if isinstance(cell, str):
                                try:
                                    val(cell)
                                    links.append(cell)
                                    next(islice(remains_enum, j, j), None)
                                    break
                                except ValidationError:
                                    print("Not a URL:", cell)
                airline_links[carrier] = links


            #fill a pandas dataframe with the info from the tables extracted above
            airline_info = []

            for (linenum, label, info) in tables:
                table_info = OrderedDict()
                table_info[page[linenum][label].value] = page[linenum][info].value
                linenum += 1
                dataline = page[linenum]

                while len(dataline) > label and dataline[label].value != 'LAST UPDATE':
                    #logging.info("dataline[label].value = {}".format(dataline[label].value))
                    if dataline[label].value != '':
                        if len(dataline) <= info:
                            table_info[dataline[label].value] = ''
                        else:
                            if dataline[label].value and (("endorsment" in dataline[label].value.lower()) or
                                                          ("waiver" in dataline[label].value.lower())):
                                table_info["Office_IATA"] = parse_office_iata(dataline[label + 1].value)

                            table_info[dataline[label].value] = dataline[info].value

                    if linenum < page.max_row:
                        linenum += 1
                        dataline = page[linenum]
                    else:
                        break

                if len(airline_links[carrier]) == 1:
                    table_info["link"] = airline_links[carrier][0]
                else:
                    table_info["link"] = None

                airline_info.append(table_info)

            airline_infos[carrier] = airline_info
    logging.getLogger().setLevel(logging.DEBUG)
    retval = pd.DataFrame({(carrier, rownum) : values for carrier, carrierlist in airline_infos.items() for rownum, values in enumerate(carrierlist)}).T

    retval["REBOOK TRAVEL DATE:"] = retval["REBOOK TRAVEL DATE:"].fillna("")
    retval["REBOOK TRAVEL DATE"] = retval.apply(lambda x: x["REBOOK TRAVEL DATE"] if x["REBOOK TRAVEL DATE"] != "" else x["REBOOK TRAVEL DATE:"], axis=1)

    retval.loc[:, ['ROUTE/ FROM/ THROUGH/ TO', 'ROUTE/ FROM/ TO', 'ROUTE/ FROM// TO',  'ROUTE/ FROM/TO']] = retval.loc[:, ['ROUTE/ FROM/ THROUGH/ TO', 'ROUTE/ FROM/ TO', 'ROUTE/ FROM// TO',  'ROUTE/ FROM/TO']].fillna("")
    retval['ROUTE/ FROM/ THROUGH/ TO'] = retval.apply(lambda x: x['ROUTE/ FROM/ THROUGH/ TO'] if x['ROUTE/ FROM/ THROUGH/ TO'] != "" else x['ROUTE/ FROM/ TO'], axis=1)
    retval['ROUTE/ FROM/ THROUGH/ TO'] = retval.apply(lambda x: x['ROUTE/ FROM/ THROUGH/ TO'] if x['ROUTE/ FROM/ THROUGH/ TO'] != "" else x['ROUTE/ FROM// TO'], axis=1)
    retval['ROUTE/ FROM/ THROUGH/ TO'] = retval.apply(lambda x: x['ROUTE/ FROM/ THROUGH/ TO'] if x['ROUTE/ FROM/ THROUGH/ TO'] != "" else x['ROUTE/ FROM/TO'], axis=1)

    #retval.loc[:, ['LINK', 'LINK :', 'Link', 'Link :', 'Link:']] = retval.loc[:, ['LINK', 'LINK :', 'Link', 'Link :', 'Link:']].fillna("")
    #retval['LINK'] = retval.apply(lambda x: x['LINK'] if x['LINK'] != "" else x['LINK :'], axis=1)
    #retval['LINK'] = retval.apply(lambda x: x['LINK'] if x['LINK'] != "" else x['Link'], axis=1)
    #retval['LINK'] = retval.apply(lambda x: x['LINK'] if x['LINK'] != "" else x['Link :'], axis=1)
    #retval['LINK'] = retval.apply(lambda x: x['LINK'] if x['LINK'] != "" else x['Link:'], axis=1)
    drop_cols = ["REBOOK TRAVEL DATE:", 'ROUTE/ FROM/ TO', 'ROUTE/ FROM// TO',  'ROUTE/ FROM/TO', 'LINK', 'LINK :', 'Link', 'Link :', 'Link:', 'ONLY FOR INVOLUNTARY CHANGES OF FLIGTHS TO USA', 'AUTH. NUMBER', 'new in KMS', '2 review', 'up to date']
    drop_cols = list(set(drop_cols).intersection(set(retval.columns)))
    retval = retval.drop(drop_cols, axis=1)

    return retval


def parse_office_iata(value):
    iata_list = [
        "PAROP3100",
        "BEROP3981",
        "LONOP3100",
        "SYDED3100",
        "MIAED3100",
        "MILED342A",
        "BCNED3351",
        "LISED3100",
        "OSLTL3160",
        "CPHTL3302",
        "HELTL3100",
        "VIEOP3100",
        "WAWTL3100",
        "STOTL3100"
    ]

    iata_catchall = [
        "",
        "? IATA",
        "ALL",
        "ALL IATA",
        "ALL IATAs",
        "IATA",
        "IATA ALL",
        "IATA?"
    ]

    if value:
        for iata in iata_list:
            if iata in value:
                return iata
        if value in iata_catchall:
            return ""
    return None


def check_office_iatas(df):
    retval = {}
    for index, iata in df["Office_IATA"].iteritems():
        if iata is None:
            retval[index] = {"IATA": None, "BROKEN": True}
        else:
            retval[index] = {"IATA": iata, "BROKEN": False}
    return pd.DataFrame(retval).T

def parse_flights(df):
    print(df.columns)
    retval = {}
    for index, applies in df["APPLIES TO"].iteritems():
        if applies is not None:
            if isinstance(applies, str) and applies.strip() != "":
                if applies.strip() != "-":
                    if len(applies.strip()) == 2:
                        logging.debug(f"Found a non-empty application condition that works automatically: {applies}")
                        retval[index] = {"APPLIES_RESTRICTIONS": applies.strip(), "APPEND_OTHER": None, "BROKEN": False}
                    elif len(applies.strip()) > 2:
                        retval[index] = {"APPLIES_RESTRICTIONS": None, "APPEND_OTHER": "APPLIES TO:\n" + applies.strip(), "BROKEN": False}
                else:
                    retval[index] = {"APPLIES_RESTRICTIONS": None, "APPEND_OTHER": None, "BROKEN": False}
            elif isinstance(applies, numbers.Number):
                logging.debug(f"Found a non-empty application condition to FC: {applies}")
                retval[index] = {"APPLIES_RESTRICTIONS": None, "APPEND_OTHER": "APPLIES TO: " + str(applies), "BROKEN": False}
            else:
                logging.debug(f"Very weird applies to condition: {applies}")
                retval[index] = {"APPLIES_RESTRICTIONS": None, "APPEND_OTHER": None, "BROKEN": True}
        else:
            retval[index] = {"APPLIES_RESTRICTIONS": None, "APPEND_OTHER": None, "BROKEN": False}
    return pd.DataFrame(retval).T


def parse_from_to(df):
    datestrings = df['TRAVEL DATES IMPACTED']
    return parse_daterange(datestrings)

def parse_issuance(df):
    datestrings = df['DATE OF ISSUANCE']
    return parse_daterange(datestrings)

def parse_lastupdate(df):
    datestrings = df["LAST UPDATE"]
    retval = {}
    for index, d in datestrings.iteritems():
        if d is None or pd.isnull(d):
            retval[index] = {"lastUpdate": None, "LAST_UPDATE_BROKEN": True}
        elif (not isinstance(d, str)) and (not isinstance(d, datetime)):
            logging.debug(f"Not a datetime format for {index} value: {d}")
            retval[index] = {"lastUpdate": None, "LAST_UPDATE_BROKEN": True}
        else:
            if isinstance(d, datetime):
                retval[index] = {"lastUpdate": d, "LAST_UPDATE_BROKEN": False}
            else:
                if d.strip() == "" or d.strip() == "-":
                    retval[index] = {"lastUpdate": None, "LAST_UPDATE_BROKEN": True}
                else:
                    lupdate = dateparser.parse(d, settings={'DATE_ORDER': 'DMY'})
                    if lupdate is None:
                        retval[index] = {"lastUpdate": None, "LAST_UPDATE_BROKEN": True}
                    else:
                        retval[index] = {"lastUpdate": lupdate, "LAST_UPDATE_BROKEN": False}
    return pd.DataFrame(retval).T

def parse_daterange(datestrings, always_fromto=None):
    retval = {}
    for index, d in datestrings.iteritems():
        if d is None:
            retval[index] = {"FROM_DATE": None, "TO_DATE": None, "BROKEN": False}
        elif (not isinstance(d, str)) and (not isinstance(d, datetime)):
            logging.debug(f"Not a datetime format for {index} value: {d}")
            retval[index] = {"FROM_DATE": None, "TO_DATE": None, "BROKEN": True}
        else:
            if isinstance(d, datetime):
                retval[index] = {"FROM_DATE": None, "TO_DATE": d, "BROKEN": False }
            else:
                if d.strip() == "" or d.strip() == "-":
                    retval[index] = {"FROM_DATE": None, "TO_DATE": None, "BROKEN": False}
                else:
                    options = [d, d.replace(".", "-"), d.replace("-", " to "), d.replace("and", "to")]
                    date_options = map(lambda x: search_dates(x, settings={'DATE_ORDER': 'DMY'}), options)
                    start, end = None, None
                    for dates in date_options:
                        if dates is None:
                            logging.debug(f"Cannot find dates for {index } value: {d}")
                            #retval[index] = {"FROM_DATE": None, "TO_DATE": None}
                        elif len(dates) > 2:
                            logging.debug(f"Period is too complex, discarding for {index } value: {d}")
                            #retval[index] = {"FROM_DATE": None, "TO_DATE": None}
                        elif len(dates) == 0:
                            logging.debug(f"No dates found, discarding for {index}: {d}")
                        elif len(dates) == 2:
                            d1 = dates[0][1]
                            d2 = dates[1][1]
                            try:
                                if d1 < d2:
                                    #retval[index] = {"FROM_DATE": d1, "TO_DATE": d2}
                                    start, end = d1, d2
                                    break
                                else:
                                    logging.debug(f"First date: {d1} isn't smaller than second: {d2}, so this is not a period. Discarding for {index } value: {d}")
                                    #retval[index] = {"FROM_DATE": None, "TO_DATE": None}
                            except TypeError:
                                logging.debug(f"Probably parsed dates wrong as there is a datetime comparison that breaks. Discarding for {index } value: {d}")
                        else: #exactly 1 date found
                            if always_fromto == "from":
                                start = dates[0][1]
                                break
                            elif always_fromto == "to":
                                end = dates[0][1]
                                break
                            else:

                                no_fromto_identifier = True

                                before = ["before", "until", "up to"]
                                for s in before:
                                    if s in d.lower():
                                        end = dates[0][1]
                                        no_fromto_identifier = False

                                after = ["after", "starting"]
                                for s in after:
                                    if s in d.lower():
                                        start = dates[0][1]
                                        no_fromto_identifier = False
                                if no_fromto_identifier:
                                    logging.debug(f"Do not know if date is a start or end date. Discarding for {index } value: {d}")
                                else:
                                    break
                    if start is None and end is None:
                        retval[index] = {"FROM_DATE": start, "TO_DATE": end, "BROKEN": True}
                    else:
                        retval[index] = {"FROM_DATE": start, "TO_DATE": end, "BROKEN": False}

    return pd.DataFrame(retval).T


def parse_routes(df):
    routedata = df["ROUTE/ FROM/ THROUGH/ TO"]
    retval = {}
    for index, d in routedata.iteritems():
        if d is None or pd.isnull(d):
            logging.debug(f"Empty route condition for {index}")
            retval[index] = {"ROUTE_CONDITIONS": None, "ROUTE_BROKEN": False}
        else:
            if isinstance(d, str):
                if d.strip() != "":
                    if d.strip().lower() in ["-", "all", "all routes", "all cities", "all flights", "any", "any destination"]:
                        logging.debug(f"Found a non-empty route condition that works automatically: {d}")
                        retval[index] = {"ROUTE_CONDITIONS": None, "ROUTE_BROKEN": False}
                    else:
                        logging.debug(f"Route condition too complex: {d}")
                        retval[index] = {"ROUTE_CONDITIONS": d, "ROUTE_BROKEN": True}
                else:
                    logging.debug(f"Empty route condition for {index}")
                    retval[index] = {"ROUTE_CONDITIONS": None, "ROUTE_BROKEN": False}
            else:
                logging.debug(f"Very weird route condition: {d}")
                retval[index] = {"ROUTE_CONDITIONS": None, "ROUTE_BROKEN": True}

    return pd.DataFrame(retval).T
