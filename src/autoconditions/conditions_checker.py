import pandas as pd
import logging


def check_if_conditions_apply(booking, airline_data):
    booking_info = dict()

    for _, booking_row in booking.iterrows():
        logging.info("########## Booking: {}".format(booking_row.BOOKING_ID))
        logging.info("########## Carrier: {}".format(booking_row.ID_OPERATING_CARRIER))
        #idx_seg = "{}-{}".format(booking_row.IATA_CODE_SEGMENT_DEP_LOCATION, booking_row.IATA_CODE_SEGMENT_ARR_LOCATION)
        idx_seg = "{}-{}-{}".format(booking_row.IATA_CODE_SEGMENT_DEP_LOCATION,
                                    booking_row.IATA_CODE_SEGMENT_ARR_LOCATION,
                                    booking_row.OPERATING_CARRIER_FLIGHT_NUM)
        booking_info[idx_seg] = dict()
        booking_info[idx_seg]['carrier'] = booking_row.ID_OPERATING_CARRIER
        booking_info[idx_seg]['carrier_info'] = dict()

        if booking_row.ID_OPERATING_CARRIER in airline_data.index.get_level_values(0):
            logging.info("########## ------------------ booking_row")
            logging.info(booking_row)
            logging.info("########## ------------------")
            booking_info[idx_seg]['carrier_info']['info_available'] = True
            # booking_info[idx_seg]['carrier_info']['data'] = airline_data.loc[booking_row.ID_OPERATING_CARRIER]
            booking_info[idx_seg]['conditions'] = []
            for idx, airline_row in airline_data.loc[booking_row.ID_OPERATING_CARRIER].iterrows():
                booking_info[idx_seg]['conditions'] .append(airline_row)
                logging.info("########## ------------------ airline_row")
                logging.info(airline_row)
                logging.info("########## ------------------")
                booking_info[idx_seg]['carrier_info'][idx] = dict()
                print("aaaa", "bbbb" if (airline_row.FROM_DATE.tz_localize('UTC') <= booking_row.DEPARTURE_DATE) else "bla",  airline_row.TO_DATE.tz_localize('UTC'))
                if (airline_row.FROM_DATE.tz_localize('UTC') <= booking_row.DEPARTURE_DATE <=
                    airline_row.TO_DATE.tz_localize('UTC')) or (airline_row.FROM_DATE.tz_localize('UTC') <=
                        booking_row.ARRIVAL_DATE <= airline_row.TO_DATE.tz_localize('UTC')):
                    logging.info("########## Dates of booking segment are included in conditions timeframes...")
                    booking_info[idx_seg]['carrier_info'][idx]["date_included"] = True
                else:
                    logging.info("########## Dates of booking segment are NOT included in conditions timeframes...")
                    booking_info[idx_seg]['carrier_info'][idx]["date_included"] = False

                if airline_row[" HAS_FC_RESTRICTIONS"]:
                    booking_info[idx_seg]['carrier_info'][idx]["has_restrictions"] = True
                    logging.info("########## Carrier restrictions. Check if flight number is affected...")
                    flight_number = booking_row.OPERATING_CARRIER_FLIGHT_NUM.split(booking_row.ID_OPERATING_CARRIER)[1]

                    if flight_number in airline_row['APPLIES TO']:
                        logging.info("########## Flight number of booking segment is included in 'APPLIES TO'...")
                        booking_info[idx_seg]['carrier_info'][idx]["flight_number_included"] = True
                    else:
                        logging.info("########## Flight number of booking segment is NOT included in 'APPLIES TO'...")
                        booking_info[idx_seg]['carrier_info'][idx]["flight_number_included"] = False
                booking_info[idx_seg]['carrier_info'][idx]["refund_info"] = False
                booking_info[idx_seg]['carrier_info'][idx]["change_info"] = False
                if airline_row["REFUND?"] is not None:
                    if isinstance(airline_row["REFUND?"], str) and len(airline_row["REFUND?"]) > 2:
                        booking_info[idx_seg]['carrier_info'][idx]["refund_info"] = True

                if airline_row["CHANGES?"] is not None:
                    if isinstance(airline_row["REFUND?"], str) and len(airline_row["REFUND?"]) > 2:
                        booking_info[idx_seg]['carrier_info'][idx]["change_info"] = True

                else:
                    logging.info("########## Carrier doesn't have special conditions...")
                    booking_info[idx_seg]['carrier_info'][idx]["has_restrictions"] = False
                    booking_info[idx_seg]['carrier_info'][idx]["flight_number_included"] = None
        else:
            logging.info("########## Carrier from booking doesn't have special conditions that apply...")
            booking_info[idx_seg]['carrier_info']['info_available'] = False

    return booking_info
