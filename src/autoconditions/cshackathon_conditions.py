""""Main module."""
import datetime
from collections import OrderedDict

from .confluence_scraper import Scrape
from .img_to_json import convert
from .get_from_spreadsheet import get_xls, parse_flights, parse_from_to, parse_issuance, check_office_iatas, parse_routes, parse_lastupdate

import json
import logging
import os

import pandas as pd


class ConditionChecker:

    def __init__(self):
        self.airline_data = ConditionChecker.get_data()
        self.booking_data = ConditionChecker.get_bookings()
        self.translate = OrderedDict([
            ("lastUpdate", "lastUpdate"),
            ("ISSUANCE_FROM_DATE", "impactedIssuanceDateStart"),
            ("ISSUANCE_TO_DATE", "impactedIssuanceDateEnd"),
            ("TRAVEL_FROM_DATE", "impactedTravelDateStart"),
            ("TRAVEL_TO_DATE", "impactedTravelDateEnd"),
            ("APPLIES_RESTRICTIONS", "appliesToConditions"),
            ("ROUTE/ FROM/ THROUGH/ TO", "routeConditions"),
            ("WAIVER / ENDORSMENT", ["waiverCode", "endorsementCode"]),
            ("WAIVER CODE/ ENDORSEMENT:", ["waiverCode", "endorsementCode"]),
            ("WAIVER / ENDORSEMENT", ["waiverCode", "endorsementCode"]),
            ("WAIVER CODE:", "waiverCode"),
            ("IATA", "officeIATACode"),
            ("REFUND?", "refundAllowed"),
            ("CHANGES?", "changeAllowed"),
            ("CHANGES", "changeAllowed"),
            ("CHANGES:", "changeAllowed"),
            ("CHANGES ", "changeAllowed"),
            ("REROUTING ALLOWED:", "reroutingAllowed"),
            ("Rerouting allowed", "reroutingAllowed"),
            ("UPGRADES:", "upgradesAllowed"),
            ("UPGRADES", "upgradesAllowed"),
            ("OAL (codeshare / segments of other airline)", "oalCode"),
            ("REBOOK TRAVEL DATES", "rebookTravelDate"),
            ("REISSUE/ REFUND MUST BE COMPLETED:", ["reissueConditions", "refundConditions"]),
            ("REFUND", "refundConditions"),
            ("REFUND:", "refundConditions"),
            ("APPEND_OTHER", "otherInfo"),
            ("OTHER INFO:", "otherInfo"),
            ("OTHER INFO: ", "otherInfo"),
            ("OTHER INFO", "otherInfo"),
            ("EMD", "emdConditions"),
            ("INFO ABOUT EMD", "emdConditions"),
            ("NO SHOW", "noShowConditions"),
            ("link", "originalInfoLink")
        ])

    @staticmethod
    def get_data():
        if os.path.exists("data/cleaned.pkl"):
            logging.info("########## load clean data...")
            airline_data = pd.read_pickle("data/cleaned.pkl")
            return airline_data
        elif os.path.exists("data/policies.pkl"):
            logging.info("########## load and process policies data...")
            airline_data = pd.read_pickle("data/policies.pkl")
            has_restrictions = airline_data["FLIGHT_CODES"] = parse_flights(airline_data)
            from_to = parse_from_to(airline_data)
            airline_data = pd.concat([airline_data, from_to, has_restrictions], axis=1)

            pd.to_pickle(airline_data, "data/cleaned.pkl")
            #logging.info(str(airline_data.columns))
            #airline_data.loc[:, ["ORIGIN", "DESTINATION"]] = parse_routes(airline_data)
            return airline_data
        elif os.path.exists("data/policies.xlsx"):
            logging.info("Have ODS data, don't mess with images")
            airline_data = get_xls()

            applies_to_restrictions = parse_flights(airline_data)
            applies_to_restrictions.rename(columns={'BROKEN':'APPLIES_BROKEN'}, inplace=True)
            total = applies_to_restrictions.shape[0]
            logging.warning(f"All: {total}")
            brokens = applies_to_restrictions.loc[applies_to_restrictions["APPLIES_BROKEN"], :].shape[0]
            logging.warning(f"Applies broken: {brokens}, {float(brokens)/total}")

            lupdate_restrictions = parse_lastupdate(airline_data)
            brokens = lupdate_restrictions.loc[lupdate_restrictions["LAST_UPDATE_BROKEN"], :].shape[0]
            logging.warning(f"Last update broken: {brokens}, {float(brokens)/total}")


            travel_date_restrictions = parse_from_to(airline_data)
            travel_date_restrictions.rename(columns={'BROKEN':'TRAVEL_DATE_BROKEN', 'FROM_DATE':'TRAVEL_FROM_DATE', 'TO_DATE': 'TRAVEL_TO_DATE'}, inplace=True)
            brokens = travel_date_restrictions.loc[travel_date_restrictions["TRAVEL_DATE_BROKEN"], :].shape[0]
            logging.warning(f"Travel date broken: {brokens}, {float(brokens)/total}")
            issuance_restrictions = parse_issuance(airline_data)
            issuance_restrictions.rename(columns={'BROKEN':'ISSUANCE_BROKEN', 'FROM_DATE':'ISSUANCE_FROM_DATE', 'TO_DATE': 'ISSUANCE_TO_DATE'}, inplace=True)
            brokens = issuance_restrictions.loc[issuance_restrictions["ISSUANCE_BROKEN"], :].shape[0]
            logging.warning(f"Issuance date broken: {brokens}, {float(brokens)/total}")
            iata_restrictions = check_office_iatas(airline_data)
            iata_restrictions.rename(columns={'BROKEN':'IATA_BROKEN'}, inplace=True)
            brokens = iata_restrictions.loc[iata_restrictions["IATA_BROKEN"], :].shape[0]
            logging.warning(f"IATA broken: {brokens}, {float(brokens)/total}")
            route_restrictions = parse_routes(airline_data)
            brokens = route_restrictions.loc[route_restrictions["ROUTE_BROKEN"], :].shape[0]
            logging.warning(f"Route broken: {brokens}, {float(brokens)/total}")
            final = pd.concat([airline_data, lupdate_restrictions, applies_to_restrictions, travel_date_restrictions, issuance_restrictions, iata_restrictions, route_restrictions], axis=1)
            brokens = final.loc[final['LAST_UPDATE_BROKEN'] | final['APPLIES_BROKEN'] | final['TRAVEL_DATE_BROKEN'] | final['ISSUANCE_BROKEN'] | final['IATA_BROKEN'] | final['ROUTE_BROKEN'], :].shape[0]
            logging.warning(f"Something broken: {brokens}, {float(brokens)/total}")


            #logging.info(str(airline_data.columns))
            #airline_data.loc[:, ["ORIGIN", "DESTINATION"]] = parse_routes(airline_data)
            return final

        else:
            logging.info("########## ELSE, something happened...")
            try:
                conditions = json.load(open("data/conditions.json", "r"))
            except:
                credentials = json.load(open("config/credentials.json", "r"))
                scrape = Scrape(credentials)
                airline_pages = scrape.getAirlinePages()

                conditions = {}
                for carrier, url in airline_pages.items():
                    carrier = carrier.strip()
                    if "/" in carrier:
                        carrier = "-".join(map(lambda x: x.strip(), carrier.split("/")))
                    info = scrape.getConditions(carrier, url, credentials)
                    if info == None:
                        logging.info("Have to scrape conditions from " + str(carrier))
                        pass
                    else:
                        logging.info("Downloaded conditions: " + str(info))
                        conditions[carrier] = info
                json.dump(conditions, open("data/conditions.json", "w"))

            for carrier, img in conditions.items():
                print(carrier)
                stuff = convert(img)
                break

            return None

    @staticmethod
    def get_bookings():
        bookings = pd.read_csv("data/booking_segment_info_sample.csv")

        bookings.loc[(bookings.TRIP_TYPE == "ONE_WAY"), 'ARRIVAL_DATE'] = None

        date_columns = [
            'DATE_REQUEST',
            'ARRIVAL_DATE',
            'DEPARTURE_DATE'
        ]

        for date_col in date_columns:
            bookings[date_col] = pd.to_datetime(bookings[date_col], format="%Y-%m-%d %H:%M:%S %Z")

        return bookings

    def get_booking_info(self, booking_id):
        if self.airline_data is None:
            self.airline_data = ConditionChecker.get_data()

        if self.booking_data is None:
            self.booking_data = ConditionChecker.get_bookings()

        booking_lines = self.booking_data.loc[self.booking_data["BOOKING_ID"] == int(booking_id), :]
        return booking_lines

    def create_jsons(self):
        brokens = self.airline_data.loc[self.airline_data['LAST_UPDATE_BROKEN'] | self.airline_data['APPLIES_BROKEN'] | self.airline_data['TRAVEL_DATE_BROKEN'] | self.airline_data['ISSUANCE_BROKEN'] | self.airline_data['IATA_BROKEN'] | self.airline_data['ROUTE_BROKEN'], :]
        continue_with = self.airline_data.loc[list(set(self.airline_data.index) - set(brokens.index))]
        continue_with = continue_with.dropna(axis=1, how="all")
        continue_with = continue_with.drop(['AIRLINE', 'LAST_UPDATE_BROKEN', 'APPLIES_BROKEN', 'TRAVEL_DATE_BROKEN', 'ISSUANCE_BROKEN', 'IATA_BROKEN', 'ROUTE_BROKEN', 'LAST UPDATE', 'DATE OF ISSUANCE', 'TRAVEL DATES IMPACTED',
                                            'APPLIES TO', 'ROUTE/ FROM/ THROUGH/ TO', 'Office_IATA',], axis=1)
        messy_labels = continue_with.loc[:, list(set(continue_with.columns) - set(self.translate.keys()))].dropna(how="all", axis=0)
        continue_with = continue_with.drop(messy_labels.index, axis=0)
        continue_with = continue_with.dropna(axis=1, how="all")

        airlines = set(continue_with.index.get_level_values(0))
        jsons = []
        for a in airlines:
            if len(a) > 2:
                continue_with = continue_with.drop(a, axis=0, level=0)
        for index, row in continue_with.iterrows():
            retval = {}
            retval["airlineCode"] = index[0]
            well_formed = True
            for (k, v) in self.translate.items():
                if k == "lastUpdate" and pd.isnull(row[k]):
                    logging.debug(f"Lastupdate should never be empty, skipping {index}")
                    well_formed = False
                    break
                if k not in row.index or pd.isnull(row[k]):
                    value = None
                else:
                    value = row[k]

                if isinstance(v, list):
                    for label in v:
                        retval = putvalue(retval, label, value)
                else:
                    retval = putvalue(retval, v, value)
            if well_formed:
                if retval["routeConditions"] is None:
                    retval["routeConditions"] = []

                if retval["appliesToConditions"] is None:
                    retval["appliesToConditions"] = []
                else:
                    retval["appliesToConditions"] = [
                        {
                            "airlineCode": retval["appliesToConditions"]
                        }]

                #if retval["officeIATACode"] == "":
                #    retval["officeIATACode"] = None
                retval = json.dumps(retval,
                                 sort_keys=True,
                                 indent=1,
                                 default=jsondates)
                jsons.append(retval)
        return jsons

def putvalue(stringdict, label, value):
    if label in stringdict:
        if value is not None:
            prevval = stringdict[label]
            if prevval is not None:
                if len(prevval) > 0:
                    prevval += "\n\n"
                stringdict[label] = prevval + value
            else:
                stringdict[label] = value
    else:
        stringdict[label] = value
    return stringdict


def jsondates(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.strftime("%d-%m-%Y")






if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s-%(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.DEBUG)

    c = ConditionChecker()

    #airline_data["CARRIER"] = airline_data.index.get_level_values(0)
    #joined = pd.merge(airline_data, bookings, how="inner", left_on="CARRIER", right_on="ID_OPERATING_CARRIER")
    #example = joined.loc[joined["BOOKING_ID"] == 5065964339, :].drop_duplicates(subset=["CARRIER"])
    #pd.set_option('display.max_rows', 500)
    #pd.set_option('display.max_columns', 500)
    #pd.set_option('display.width', 1000)


