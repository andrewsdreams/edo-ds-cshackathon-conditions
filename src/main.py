import logging
import os
import sys

from autoconditions.conditions_checker import check_if_conditions_apply
from autoconditions.cshackathon_conditions import ConditionChecker
from flask import Flask, render_template, request

if __name__ == "__main__":
    UPLOAD_TO_SYSTEM = False


    logging.basicConfig(format='%(asctime)s-%(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.WARN)

    template_dir = os.path.abspath('./web/templates')
    static_url = ''
    static_dir = os.path.abspath('./web/static')
    app = Flask(__name__, template_folder=template_dir, static_url_path=static_url, static_folder=static_dir)

    checker = ConditionChecker()

    if UPLOAD_TO_SYSTEM:
        jsons = checker.create_jsons()

        url = "http://dcd-franciscoapesteguia-airline-conditions.app-qa.services.odigeo.com:8080/airline-conditions/airlineconditions/management/v1/"
        create_action = "create"
        headers = {"Content-Type": "application/json"}
        import requests
        for doc in jsons:
            r_create = requests.post(url=url + create_action, data=doc, verify=False, headers=headers)
            if r_create.status_code != 204:
                logging.error(f"Failed on: {doc} for reason {r_create.reason}")
            else:
                logging.debug(f"Successfully added {doc}")


    @app.route("/")
    def home():
        return render_template("home.html")

    # @app.route('/validate')
    # def validate():
    #     form = BookingForm()
    #     return render_template('validate_booking.html', title='Validate Booking', form=form)

    @app.template_filter()
    def tabFormat(value):
        splitted = value.split("-")
        return f'{splitted[2]}: {splitted[0]}-{splitted[1]}'

    @app.route("/create/")
    def create():
        return render_template("create.html")

    @app.route("/issuance/")
    def issuance():
        return render_template("issuance.html")

    @app.route("/travel/")
    def travel():
        return render_template("travel.html")

    @app.route("/route/")
    def route_restrictions():
        return render_template("route.html")

    @app.route("/applies/")
    def applies_to():
        return render_template("applies.html")

    @app.route("/validate/", methods=['post'])
    def validate():
        if request.method == 'POST':
            return request.form
        else:
            return "should be post";



    @app.template_filter()
    def tabFormat(value):
        splitted = value.split("-")
        return f'{splitted[2]}: {splitted[0]}-{splitted[1]}'



    @app.route('/check/', methods=['post', 'get'])
    def check():
        message = ''
        global checker
        if request.method == 'POST':
            booking_id = request.form.get('booking_id')  # access the data inside
            if booking_id:

                dataframe = checker.get_booking_info(booking_id).reset_index(drop=True)
                if dataframe is not None and len(dataframe) > 0:
                    carrier_info = check_if_conditions_apply(dataframe, checker.airline_data)
                    has_info=False
                    if len(carrier_info) > 0:

                        dataframe = dataframe.loc[:, ["BOOKING_ID", "STATUS","TRIP_TYPE", "ARRIVAL_CITY_IATA", "DEPARTURE_CITY_IATA", "DEPARTURE_DATE", "ARRIVAL_DATE", "GROSSSALES"]].drop_duplicates().T.to_dict()[0]
                        print(dataframe)
                        dataframe["DEPARTURE_DATE"] =(dataframe["DEPARTURE_DATE"]).to_pydatetime()
                        dataframe["ARRIVAL_DATE"] = (dataframe["ARRIVAL_DATE"]).to_pydatetime()
                        refund_button = False
                        refund_hover = ""

                        change_button = False
                        change_hover = ""
                        for flight_info, v in carrier_info.items():
                            cv = v["carrier_info"]
                            for idx, specifics in cv.items():
                                if idx == "info_available":
                                    if specifics:
                                        has_info=True
                                else:
                                    if specifics["refund_info"] and "REFUND?" in v["conditions"][idx]:
                                        identifier = " --- " + str(idx +1) if len(cv) > 2 else ""
                                        refund_hover = refund_hover + "<p><b>" + tabFormat(flight_info) + identifier + "</b><br />" + v["conditions"][idx]["REFUND?"] + "</p>"
                                        refund_button = True
                                    if specifics["change_info"] and "CHANGES?" in v["conditions"][idx]:
                                        identifier = " --- " + str(idx +1) if len(cv) > 2 else ""
                                        change_hover = change_hover + "<p><b>" + tabFormat(flight_info) + identifier + "</b><br />" + v["conditions"][idx]["CHANGES?"] + "</p>"
                                        change_button = True

                        return render_template('validate_conditions_booking.html', has_carrier_info=has_info, carrierinfo=carrier_info, bookingdata=dataframe, refund=(refund_button, refund_hover), change=(change_button, change_hover))

                #otherwise
                message = "Not a valid booking ID, please try again"
                return render_template('home.html', message=message)
            else:
                message = "Not a valid booking ID, please try again"
                return render_template('home.html', message=message)
        if request.method == 'GET':
            return render_template('home.html')




    # from flask_wtf import FlaskForm
    # from wtforms import StringField, IntegerField
    # from wtforms.validators import DataRequired
    #
    #
    # class MyForm(FlaskForm):
    #     name = StringField('name', validators=[DataRequired()])
    #
    #
    # @app.route("/", methods=['GET', 'POST'])
    # def hello():
    #     form = ReusableForm(request.form)
    #
    #     print
    #     form.errors
    #     if request.method == 'POST':
    #         name = request.form['name']
    #         print
    #         name
    #
    #     if form.validate():
    #         # Save the comment here.
    #         flash('Hello ' + name)
    #     else:
    #         flash('All the form fields are required. ')
    #
    #     return render_template('hello.html', form=form)
    #
    #
    # if __name__ == "__main__":
    #     app.run(debug=True)

    app.run(debug=True)