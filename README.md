# edo-ds-cshackathon-conditions
![](https://img.shields.io/badge/version-v0.0.1-blue.svg?style=for-the-badge)
[![Docs](https://img.shields.io/badge/docs-confluence-013A97)]()

__Setup:__

* create data and config directories
* create config/credentials.json with your username and password

__Run:__

Run autoconditions.cshackathon_conditions. If you haven't donwnloaded the condition files yet, this will do that first. OCR is a work-in-progress.
