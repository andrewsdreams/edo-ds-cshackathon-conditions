import json, requests

url = "http://dcd-enricobazzoli-airline-conditions.app-qa.services.odigeo.com:8080/airline-conditions/airlineconditions/management/v1/list"

import requests
import json
url_list = "http://dcd-enricobazzoli-airline-conditions.app-qa.services.odigeo.com:8080/airline-conditions/airlineconditions/management/v1/list"
data_list_filters = {"airlineCode": None, "iataCode": None, "startUpdateTime": None, "endUpdateTime": None, "isDeprecated": True}
headers = {"Content-Type": "application/json"}
r_list = requests.post(url=url_list, data=json.dumps(data_list_filters), verify=False, headers=headers)
print(r_list)
content = json.loads(r_list.content)
print(content)
url_create = "http://dcd-enricobazzoli-airline-conditions.app-qa.services.odigeo.com:8080/airline-conditions/airlineconditions/management/v1/create"
data_create = {'airlineCode': 'AH',
               'appliesToConditions': [{'airlineCode': 'TG', 'flightCode': '217'},
                                       {'airlineCode': 'MS', 'flightCode': None}],
               'attachmentLinks': [{'description': None,
                                    'link': 'https://jira.odigeo.com/attachments/policy.pdf'}],
               'changeAllowed': 'Yes - Fare difference applies, if any',
               'endorsementCode': None,
               'impactedIssuanceDateEnd': '07-03-2020',
               'impactedIssuanceDateStart': None,
               'impactedTravelDateEnd': None,
               'impactedTravelDateStart': '31-05-2020',
               'noShowConditions': None,
               'oalCode': 'OS/LH',
               'officeIATACode': 'PAROP3107',
               'originalInfoLink': 'https://www.omanair.com/es/en/travel-advisory-covid-19-coronavirus-outbreak',
               'otherInfo': 'The policy does not apply to no-show passengers',
               'rebookTravelDate': 'According to ticker validity or until 2020-12-31',
               'refundAllowed': 'No refunds authorized - Only voucher',
               'refundConditions': None,
               'reissueConditions': 'Before original departure',
               'reroutingAllowed': 'Yes - Only on TAP flights',
               'routeConditions': [{'from': 'Tokyo', 'locationType': 'CITY'},
                                   {'locationType': 'COUNTRY', 'to': 'Japan'},
                                   {'locationType': 'AIRPORT', 'through': 'AVG'},
                                   {'from': 'Beijing', 'locationType': 'CITY', 'to': 'Tokyo'}],
               'upgradesAllowed': None,
               'waiverCode': 'CV20US'}
r_create = requests.post(url=url_create, data=json.dumps(data_create), verify=False, headers=headers)
url_deprecate = "http://dcd-enricobazzoli-airline-conditions.app-qa.services.odigeo.com:8080/airline-conditions/airlineconditions/management/v1/deprecate/2"
r_deprecate = requests.post(url=url_deprecate, verify=False, headers=headers)
url_replace = "http://dcd-enricobazzoli-airline-conditions.app-qa.services.odigeo.com:8080/airline-conditions/airlineconditions/management/v1/replace/1"
data_replace = {'airlineCode': 'WY',
                'appliesToConditions': [{'airlineCode': 'TG', 'flightCode': '217'},
                                        {'airlineCode': 'MS', 'flightCode': None}],
                'attachmentLinks': [{'description': 'Very useful link',
                                     'link': 'https://jira.odigeo.com/attachments/policy.pdf'}],
                'changeAllowed': 'Yes - Fare difference applies, if any',
                'endorsementCode': None,
                'impactedIssuanceDateEnd': '07-03-2020',
                'impactedIssuanceDateStart': '02-03-2020',
                'impactedTravelDateEnd': '25-09-2020',
                'impactedTravelDateStart': '31-05-2020',
                'noShowConditions': None,
                'oalCode': 'OS/LH',
                'officeIATACode': 'PAROP3120',
                'originalInfoLink': 'https://www.omanair.com/es/en/travel-advisory-covid-19-coronavirus-outbreak',
                'otherInfo': 'The policy does not apply to no-show passengers',
                'rebookTravelDate': 'According to ticker validity or until 2020-12-31',
                'refundAllowed': 'No refunds authorized - Only voucher',
                'refundConditions': None,
                'reissueConditions': 'Before original departure',
                'reroutingAllowed': 'Yes - Only on TAP flights',
                'routeConditions': [{'from': 'Tokyo', 'locationType': 'CITY'},
                                    {'locationType': 'COUNTRY', 'to': 'Japan'},
                                    {'locationType': 'AIRPORT', 'through': 'AVG'},
                                    {'from': 'Beijing', 'locationType': 'CITY', 'to': 'Tokyo'}],
                'upgradesAllowed': None,
                'waiverCode': 'CV20US'}
r_replace = requests.post(url=url_replace, data=json.dumps(data_replace), verify=False, headers=headers)
url_update = "http://dcd-enricobazzoli-airline-conditions.app-qa.services.odigeo.com:8080/airline-conditions/airlineconditions/management/v1/2"
data_update = {'airlineCode': 'WY',
               'appliesToConditions': [{'airlineCode': 'TG', 'flightCode': '217'},
                                       {'airlineCode': 'MS', 'flightCode': None}],
               'attachmentLinks': [{'description': 'Very useful link',
                                    'link': 'https://jira.odigeo.com/attachments/policy.pdf'}],
               'changeAllowed': 'Yes - Fare difference applies, if any',
               'endorsementCode': None,
               'impactedIssuanceDateEnd': '07-03-2020',
               'impactedIssuanceDateStart': '02-03-2020',
               'impactedTravelDateEnd': '25-09-2020',
               'impactedTravelDateStart': '31-05-2020',
               'noShowConditions': None,
               'oalCode': 'OS/LH',
               'officeIATACode': 'PAROP3101',
               'originalInfoLink': 'https://www.omanair.com/es/en/travel-advisory-covid-19-coronavirus-outbreak',
               'otherInfo': 'The policy does not apply to no-show passengers',
               'rebookTravelDate': 'According to ticker validity or until 2020-12-31',
               'refundAllowed': 'No refunds authorized - Only voucher',
               'refundConditions': None,
               'reissueConditions': 'Before original departure',
               'reroutingAllowed': 'Yes - Only on TAP flights',
               'routeConditions': [{'from': 'Tokyo', 'locationType': 'CITY'},
                                   {'locationType': 'COUNTRY', 'to': 'Japan'},
                                   {'locationType': 'AIRPORT', 'through': 'AVG'},
                                   {'from': 'Beijing', 'locationType': 'CITY', 'to': 'Tokyo'}],
               'upgradesAllowed': None,
               'waiverCode': 'CV20US'}
r_update = requests.put(url=url_update, data=json.dumps(data_update), verify=False, headers=headers)





