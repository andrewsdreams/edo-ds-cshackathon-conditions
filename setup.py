"""Setup script."""
import glob
import os

import setuptools

this_directory = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(this_directory, 'README.md')) as f:
    long_description = f.read()


def get_requirements(filename):
    with open(filename) as f:
        requirements = f.readlines()
    return requirements


requirements = get_requirements(os.path.join(this_directory, 'requirements.txt'))
edo_requirements = get_requirements(os.path.join(this_directory, 'requirements-edo.txt'))

setuptools.setup(name='edo-ds-cshackathon-conditions',
                 version='0.0.1',
                 description='Scrape airline conditions frence and structure in a way to facilitate automation',
                 long_description=long_description,
                 classifiers=[
                     "Development Status :: 2 - Pre-Alpha",
                     "Programming Language :: Python :: 3 :: Only",
                     "Programming Language :: Python :: 3.6"
                 ],
                 keywords='scrape customer_service hackathon',
                 url='http://bitbucket.org/odigeoteam/edo-ds-cshackathon-conditions',
                 author='eDreams ODIGEO',
                 author_email='mail',
                 license='COPYRIGHT',
                 packages=setuptools.find_packages('src'),
                 package_dir={'': 'src'},
                 namespace_packages=['edo'],
                 py_modules=[os.path.splitext(os.path.basename(path))[0] for path in glob.glob('src/*.py')],
                 include_package_data=True,
                 install_requires=requirements,
                 extras_require={'full': edo_requirements}

                 )
